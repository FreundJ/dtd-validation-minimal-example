from lxml import etree

parser = etree.XMLParser(dtd_validation=True, encoding='utf-8', no_network=True)

for file in ["dummy_1.xml", "dummy_2.xml"]:
    print(f"##### Check {file}:")
    try:
        etree.parse(f"journalpub-oasis-dtd-3.0/publishing-oasis/{file}", parser)
        print("File is dtd-valid.")
    except Exception as error:
        print("File is NOT dtd-valid.")
        print(error)