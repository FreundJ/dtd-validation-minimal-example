Hello world,

we intend to validate xml files against their DTD using python and lxml. We noticed several cases in which no DTD-validation was performed, as soon as "<?xmltex" occurs in the document or generalized, if the command starts with "<?xmlX", where X is any character except a blank. Such a command seems to suspend the DTD-validation, which happens quietly, since no errors occur. In order to provide you a minimal example, we strongly simplified an example xml. Originally we are talking about scientific articles. 

You can find the minmal example at https://gitlab.ulb.tu-darmstadt.de/FreundJ/dtd-validation-minimal-example. It contains a real DTD (taken from https://ftp.ncbi.nih.gov/pub/archive_dtd/publishing/journalpub-oasis-dtd-3.0.zip) and two invalid XML files:

1) dummy_1.xml: An invalid file with an "<?xmlX" element.

2) dummy_2.xml: The same invalid file without an "<?xmlX" element.

For file 2, lxml checks the validity and finds and reports the error. For file 1, nothing happens. If "<?xmlX" occurs in the xml file why lxml does no DTD-validation but suggest it? Is this a bug?

PS: At the moment, we fix that problem by replacing "<?xml" with "<?dummy". It seems to fix the problem, but we dont know conclusively. We would prefer to bypass this circumstance.

  
  
Bug tracker report  
Python              : sys.version_info(major=3, minor=9, micro=16, releaselevel='final', serial=0)  
lxml.etree          : (4, 9, 1, 0)  
libxml used         : (2, 9, 14)  
libxml compiled     : (2, 9, 14)  
libxslt used        : (1, 1, 35)  
libxslt compiled    : (1, 1, 35)  
